import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;

public class PegawaiApp extends JFrame {
    private Connection connection;
    private JTable table;
    private DefaultTableModel tableModel;
    private JTextField idField;
    private JTextField namaField;
    private JTextField umurField;
    private JTextField alamatField;

    public PegawaiApp() {
        try {
            // Initialize MySQL connection
            connection = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/Pegawai", "root", "ayammcdenak"
            );
        } catch (SQLException e) {
            e.printStackTrace();
            System.exit(1);
        }

        setTitle("Pegawai Database");
        setSize(800, 600);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);

        tableModel = new DefaultTableModel(new String[]{"ID Pegawai", "Nama", "Umur", "Alamat"}, 0);
        table = new JTable(tableModel);

        JScrollPane scrollPane = new JScrollPane(table);
        add(scrollPane, BorderLayout.CENTER);

        JPanel inputPanel = new JPanel();
        inputPanel.setLayout(new GridLayout(5, 2));

        inputPanel.add(new JLabel("ID Pegawai:"));
        idField = new JTextField();
        inputPanel.add(idField);

        inputPanel.add(new JLabel("Nama:"));
        namaField = new JTextField();
        inputPanel.add(namaField);

        inputPanel.add(new JLabel("Umur:"));
        umurField = new JTextField();
        inputPanel.add(umurField);

        inputPanel.add(new JLabel("Alamat:"));
        alamatField = new JTextField();
        inputPanel.add(alamatField);

        JButton addButton = new JButton("Add Pegawai");
        inputPanel.add(addButton);

        JButton loadButton = new JButton("Load Pegawai");
        inputPanel.add(loadButton);

        add(inputPanel, BorderLayout.SOUTH);

        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addPegawai();
            }
        });

        loadButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                loadPegawai();
            }
        });
    }

    private void addPegawai() {
        String id = idField.getText();
        String nama = namaField.getText();
        int umur;
        try {
            umur = Integer.parseInt(umurField.getText());
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(this, "Umur harus berupa angka", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }
        String alamat = alamatField.getText();

        if (id.isEmpty() || nama.isEmpty() || alamat.isEmpty()) {
            JOptionPane.showMessageDialog(this, "ID, Nama, dan Alamat tidak boleh kosong", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }

        try (PreparedStatement statement = connection.prepareStatement("INSERT INTO Pegawai (ID_PEGAWAI, NAMA, UMUR, ALAMAT) VALUES (?, ?, ?, ?)")) {
            statement.setString(1, id);
            statement.setString(2, nama);
            statement.setInt(3, umur);
            statement.setString(4, alamat);
            statement.executeUpdate();
            JOptionPane.showMessageDialog(this, "Pegawai added successfully");
            idField.setText("");
            namaField.setText("");
            umurField.setText("");
            alamatField.setText("");
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this, "Failed to add Pegawai", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void loadPegawai() {
        tableModel.setRowCount(0);

        try (Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery("SELECT * FROM Pegawai")) {
            while (resultSet.next()) {
                String id = resultSet.getString("ID_PEGAWAI");
                String nama = resultSet.getString("NAMA");
                int umur = resultSet.getInt("UMUR");
                String alamat = resultSet.getString("ALAMAT");
                tableModel.addRow(new Object[]{id, nama, umur, alamat});
            }
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this, "Failed to load Pegawai", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new PegawaiApp().setVisible(true);
            }
        });
    }
}
